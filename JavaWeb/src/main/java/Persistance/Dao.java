package Persistance;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Model.Consulta;

import com.mysql.cj.jdbc.MysqlDataSource;

public class Dao {
	
	private static Dao instance;
	
	String URL;
	String user;
	String password;
	
	Connection conn;
	
	
	public static Dao getInstance() throws ClassNotFoundException, SQLException {
        if (instance == null) {
            synchronized (Dao.class) {
                if (instance == null) {
                    instance = new Dao();
                }
            }
        }
        return instance;
    }
	
	public void connect () {
		
		URL = "jdbc:mysql://localhost:3306/dam2tm06uf4p1";
		user = "root";
		password = "";
		
		try {
			conn = (Connection) DriverManager.getConnection(URL, user, password);
			System.out.println("Successfull");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	public Connection getConnection() {
		return conn;
	}
	
	public void closeConnection() throws SQLException {
		conn.close();
	}
	
	private Dao () throws SQLException, ClassNotFoundException {
		Class.forName("com.mysql.cj.jdbc.Driver");
		connect();
	}
	
	public List<Consulta> alumnosNoMarcados() throws SQLException {
		
		ArrayList<Consulta> consultaList = new ArrayList<Consulta>();
		
		String query = "SELECT * FROM consultas WHERE marcado = 0";
		PreparedStatement newStatement = conn.prepareStatement(query);

		ResultSet result = newStatement.executeQuery();
		
		while (result.next()) {
			
			int id = result.getInt("id");
			String name = result.getString("nombre");
			boolean marcado = result.getBoolean("marcado");
			Consulta cons = new Consulta(id, name, marcado);

			consultaList.add(cons);
		}
		return consultaList;
	}

	public void marcarAlumno(int id) throws SQLException {
		
		String query = "UPDATE consultas " + "SET marcado = 1 " + "WHERE id = ?";
		PreparedStatement newStatement;
		newStatement = conn.prepareStatement(query);
		newStatement.setInt(1, id);
		newStatement.executeUpdate();
	}
	
	public boolean comprobarMarcado(int id) throws SQLException {
		
		String query = "SELECT marcado "
				+ "FROM consultas"
				+ "WHERE id = ?";
		PreparedStatement newStatement = conn.prepareStatement(query);
		newStatement.setInt(1, id);
		ResultSet result = newStatement.executeQuery();
		return result.next();
	}
	
	public void deleteAlumno(int id) throws SQLException {
		
		String query = "DELETE FROM consultas"
				+ "WHERE id = ?";
		PreparedStatement newStatement = conn.prepareStatement(query);
		newStatement.setInt(1, id);
		newStatement.execute();
	}
	
}
