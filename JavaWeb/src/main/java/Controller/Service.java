package Controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Model.Consulta;
import Persistance.Dao;

public class Service {

	HttpServletRequest request;
	HttpServletResponse response;
	Dao dao;
	
	public Service (HttpServletRequest request, HttpServletResponse response) throws ClassNotFoundException, SQLException {
		
		this.dao = Dao.getInstance();
		this.request = request;
		this.response = response;
	}
	
	public void closeConnection() throws SQLException, ClassNotFoundException {
		Dao.getInstance().closeConnection();
	}
	
	public void checkAll() {
		try {
			ArrayList<Consulta> result = new ArrayList<Consulta>(dao.alumnosNoMarcados());
			System.out.println("This is the result" + result.toString());
			request.setAttribute("arrayResult", result);
			request.getRequestDispatcher("turnoenclase.jsp").forward(request, response);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void checkAlumno() {
		try {
			//Get the user ID to check in the DB
			int userId = Integer.parseInt(request.getParameter("alumnoId"));
			dao.marcarAlumno(userId);
			System.out.println("User" + userId + " checked");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void sendToRegisterForm() {
		try {
			request.getRequestDispatcher("turnodeclase.jsp").forward(request, response);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void insertOrUpdateUser() {
		try {
			
			request.getRequestDispatcher("turnodeclase.jsp").forward(request, response);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
