package Model;

public class Consulta {
	
	int id;
	String nombre;
	Boolean marcado;
	
	
	public Consulta(String nombre, Boolean marcado) {
		this.nombre = nombre;
		this.marcado = marcado;
	}
	
	public Consulta(int id, String name, boolean check) {
		super();
		this.id = id;
		this.nombre = name;
		this.marcado = check;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Boolean getMarcado() {
		return marcado;
	}

	public void setMarcado(Boolean marcado) {
		this.marcado = marcado;
	}


	
	
	
	
}
